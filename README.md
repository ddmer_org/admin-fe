项目初始化步骤

1.安装nodejs环境,推荐使用v4.4.7
    下载地址 : https://nodejs.org/download/release/v4.4.7/

2.gitee下载源码
    下载地址 : https://gitee.com/lenoorg/admin-fe.git

3.在项目根目录执行npm初始化http://wz76711.cn:3000/list.html?categoryId=100014
  npm config get prefix
  npm config set registry=https://registry.npm.taobao.org
  npm config list
    命令: npm install (--registry=https://registry.npm.taobao.org)

默认统一安装的simditor可能是2.3.28，后面项目启动会有一个错误:
Uncaught TypeError: Simditor.connect is not a function
手动安装2.3.6的版本
npm install simditor@2.3.6

4.启动项目
    开发模式: npm run dev (windows系统上为npm run dev_win)
    生产模式: npm run dist (windows系统上为npm run dist_win)

5.开发模式下预览项目
    访问：http://localhost:8086/dist/view/index.html


注意：
1.后台管理系统使用了sass, 可能需要安装ruby和sass

2.后台管理系统的预览账号：admin  111111